<?php

declare(strict_types=1);

namespace App\Entity;

interface CanStoreEventInterface
{
    public function getId(): ?int;
}
