<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Company;
use App\Form\CompanyType;
use App\Repository\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractController
{
    public function __construct(private readonly CompanyRepository $companyRepository)
    {
    }

    #[Route('/', name: 'app_company')]
    public function index(): Response
    {
        return $this->render('company/index.html.twig', [
            'companies' => $this->companyRepository->findAll()
        ]);
    }

    #[Route('/company/show/{id}', name: 'app_company_show')]
    public function show(Company $company): Response
    {
        return $this->render(
            'company/show.html.twig',
            [
                'company' => $company
            ]
        );
    }

    #[Route('/company/create', name: 'app_company_create')]
    public function create(Request $request): Response
    {
        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $addresses = $form->get('addresses')->getData();
            foreach ($addresses as $address) {
                $address->setCompany($company);
            }

            $this->companyRepository->save($company, true);

            return $this->redirectToRoute('app_company');
        }

        return $this->render(
            'company/create.html.twig',
            [
                'form' => $form
            ]
        );
    }

    #[Route('/company/update/{id}', name: 'app_company_update')]
    public function update(Company $company, Request $request): Response
    {
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $addresses = $form->get('addresses')->getData();
            foreach ($addresses as $address) {
                if (is_null($address->getCompany())) {
                    $address->setCompany($company);
                }
            }

            $this->companyRepository->flush();

            return $this->redirectToRoute('app_company');
        }

        return $this->render(
            'company/update.html.twig',
            [
                'form' => $form
            ]
        );
    }

    #[Route('/company/delete/{id}', name: 'app_company_delete')]
    public function delete(Company $company): Response
    {
        $this->companyRepository->remove($company, true);

        return $this->redirectToRoute('app_company');
    }
}
