<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\EventRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends AbstractController
{
    public function __construct(private readonly EventRepository $eventRepository)
    {
    }

    /**
     * @throws Exception
     */
    #[Route('/event/created_at', name: 'app_event_created_at')]
    public function getEventByDate(Request $request): JsonResponse
    {
        $date = $request->query->get('date');
        $criteria = [
            'company' => $request->query->get('companyId'),
        ];

        if (!empty($date)) {
            $criteria['createdAt'] = new \DateTimeImmutable(\DateTime::createFromFormat("d/m/Y H:i:s", $date)
                ->format("Y-m-d H:i:s"));
        }

        $event = $this->eventRepository->findOneBy($criteria, ['createdAt' => 'DESC']);

        if ($event === null) {
            return new JsonResponse(
                [
                    'message' => 'Aucun événement trouvé pour cette date'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        $responseData = [
            'id' => $event->getId(),
            'eventType' => $event->getEventType(),
            'entity' => $event->getEntity(),
            'changes' => $event->getChanges(),
        ];

        return $this->json($responseData);
    }
}
