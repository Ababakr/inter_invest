<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\LegalForm;
use App\Repository\LegalFormRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class LegalFormImportCommand extends Command
{
    protected static $defaultName = 'app:import:legal-form';

    public function __construct(
        string $projectDir,
        private readonly LegalFormRepository $legalFormRepository
    ) {
        $this->projectDir = $projectDir;
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->setDescription('Import legal form infos')
            ->addArgument('fileName', InputArgument::REQUIRED, 'Name of import file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = $input->getArgument('fileName');
        $rowsDatas = $this->getCsvRowsAsArrays($fileName);

        $newLegalFormCount = 0;

        foreach ($rowsDatas as $data) {
            $this->createNewLegalForm($data);
            $newLegalFormCount++;
        }

        $this->legalFormRepository->flush();
        $io = new SymfonyStyle($input, $output);
        $io->success("$newLegalFormCount new legalForm have beeen added");

        return Command::SUCCESS;

    }

    private function getCsvRowsAsArrays(string $fileName): mixed
    {
        $filePath =  $this->projectDir. '/public/imports/'.$fileName;

        $decoder = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        return $decoder->decode(file_get_contents($filePath), 'csv', [CsvEncoder::DELIMITER_KEY => ';']);
    }

    private function createNewLegalForm(array $data): void
    {
        $legaForm = (new LegalForm())
            ->setLabel($data['libelle'])
        ;

        $this->legalFormRepository->save($legaForm);

    }
}
