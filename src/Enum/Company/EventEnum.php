<?php

declare(strict_types=1);

namespace App\Enum\Company;

enum EventEnum : string
{
    case CREATE = 'Création';
    case UPDATE = 'Modification';
}
