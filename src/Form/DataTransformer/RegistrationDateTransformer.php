<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use DateTimeImmutable;
use Exception;
use Symfony\Component\Form\DataTransformerInterface;

class RegistrationDateTransformer implements DataTransformerInterface
{

    public function transform(mixed $value): string
    {
        if (null != $value) {
            /** @var DateTimeImmutable $date */
            $date = $value;

            return $date->format('d/m/Y');
        }

        return '';
    }

    /**
     * @throws Exception
     */
    public function reverseTransform(mixed $value): ?DateTimeImmutable
    {
        if (null === $value) {
            return null;
        }

        $date = date_create_from_format("d/m/Y", $value)->format("Y-m-d");

        return new DateTimeImmutable($date);
    }
}