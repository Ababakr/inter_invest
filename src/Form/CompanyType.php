<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\LegalForm;
use App\Form\DataTransformer\RegistrationDateTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    public function __construct(private readonly RegistrationDateTransformer $registrationDateTransformer)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label_format' => "Nom de l'entreprise",
                'label_attr' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('sirenNumber', IntegerType::class, [
                'label_format' => 'Numéro de siren',
                'label_attr' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add(
                'registrationCity',
                TextType::class,
                [
                'label_format' => "Ville d'immatriculation",
                'label_attr' => [
                    'class' => 'col-sm-2 col-form-label',
                ],
                'attr' => [
                    'class' => 'form-control'
                ]
                ]
            )
            ->add(
                'registrationDate',
                TextType::class,
                [
                    'label' => "Date d'immatriculation",
                    'label_attr' => [
                        'class' => 'col-sm-2 col-form-label',
                    ],
                    'attr' => [
                        'class' => 'datepicker form-control'
                    ]
                ]
            )
            ->add(
                'legalForm',
                EntityType::class,
                [
                    'label' => 'Forme juridique',
                    'class' => LegalForm::class,
                    'choice_label' => 'label',
                    'placeholder' => 'Sélectionnez une forme juridique',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]
            )
            ->add(
                'addresses',
                CollectionType::class,
                [
                    'entry_type' => AddressType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false
                ]
            )
            ->add(
                'register',
                SubmitType::class,
                [
                    'label' => "Enregistrer",
                    'attr' => [
                        'class' => 'btn btn-primary btn-sm'
                    ]
                ]
            )
        ;

        $builder->get('registrationDate')->addModelTransformer($this->registrationDateTransformer);
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
    }

    public function onPostSubmit(FormEvent $event)
    {
        $companyForm = $event->getForm();
        $company = $companyForm->getData();

        if(count($company->getAddresses()) == 0) {
            $companyForm->get('addresses')->addError(new FormError("One address is required"));
        }

        if (empty($company->getLegalForm())) {
            $companyForm->get('legalForm')->addError(new FormError("One legalForm is required"));
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
