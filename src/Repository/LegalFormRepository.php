<?php

namespace App\Repository;

use App\Entity\LegalForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LegalForm>
 *
 * @method LegalForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method LegalForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method LegalForm[]    findAll()
 * @method LegalForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegalFormRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LegalForm::class);
    }

    public function save(LegalForm $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LegalForm $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }

}
