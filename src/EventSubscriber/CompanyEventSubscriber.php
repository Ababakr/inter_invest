<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Address;
use App\Entity\Company;
use App\Handler\EventHandlerManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class CompanyEventSubscriber implements EventSubscriber
{
    public function __construct(private readonly EventHandlerManager $eventHandlerManager)
    {
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Company || $entity instanceof Address) {
            $this->eventHandlerManager->logCreate($entity);
        }
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Company || $entity instanceof Address) {
            $this->eventHandlerManager->logUpdate($entity);
        }
    }
}
