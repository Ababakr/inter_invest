<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\CanStoreEventInterface;
use App\Entity\Company;
use App\Entity\Event;
use App\Enum\Company\EventEnum;
use App\Repository\EventRepository;
use App\Serializer\CompanySerializer;
use DateTimeImmutable;
use ReflectionClass;

class EventHandlerManager
{
    public function __construct(
        private readonly EventRepository $eventRepository,
        private readonly CompanySerializer $serializer
    ) {
    }

    public function logCreate(CanStoreEventInterface $entity): void
    {
        $this->createEvent($entity, EventEnum::CREATE->value);
    }

    public function logUpdate(CanStoreEventInterface $entity): void
    {
        $this->createEvent($entity, EventEnum::UPDATE->value);
    }

    private function createEvent(CanStoreEventInterface $entity, string $eventType): void
    {
        $entityName = (new ReflectionClass($entity))->getShortName();
        $event = (new Event())
            ->setEventType($eventType)
            ->setCompany($entity instanceof Company ? $entity : $entity->getCompany())
            ->setEntity($entityName)
            ->setChanges($this->serializer->serialize($entity instanceof Company ? $entity : $entity->getCompany()))
            ->setCreatedAt(new DateTimeImmutable())
        ;

        $this->eventRepository->save($event, true);
    }
}
