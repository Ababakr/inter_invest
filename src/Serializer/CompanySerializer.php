<?php

declare(strict_types=1);

namespace App\Serializer;

use App\Entity\Company;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Serializer\SerializerInterface;

class CompanySerializer
{
    public function __construct(private readonly SerializerInterface $serializer)
    {
    }

    public function serialize(Company $company): array
    {
        return $this->serializer->normalize(
            $company,
            null,
            [
                'groups' => 'company',
            ]
        );
    }
}
