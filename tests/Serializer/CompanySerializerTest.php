<?php

declare(strict_types=1);

namespace App\Tests\Serializer;

use App\Entity\Address;
use App\Entity\Company;
use App\Entity\LegalForm;
use App\Serializer\CompanySerializer;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class CompanySerializerTest extends TestCase
{
    private function setPrivateProperty(object $object, string $propertyName, $value): void
    {
        $reflectionClass = new ReflectionClass($object);
        $reflectionProperty = $reflectionClass->getProperty($propertyName);
        $reflectionProperty->setAccessible(true);

        if ($reflectionProperty->getType()->getName() === 'int') {
            $reflectionProperty->setValue($object, (int) $value);
        } else {
            $reflectionProperty->setValue($object, $value);
        }
    }

    public function testSerialize(): void
    {
        $legalForm = new LegalForm();
        $this->setPrivateProperty($legalForm, 'id', 1);
        $this->setPrivateProperty($legalForm, 'label', 'Example Legal Form');

        $address1 = new Address();
        $this->setPrivateProperty($address1, 'id', 1);
        $this->setPrivateProperty($address1, 'number', 123);
        $this->setPrivateProperty($address1, 'streetType', 'Example Street Type');
        $this->setPrivateProperty($address1, 'streetName', 'Example Street Name');
        $this->setPrivateProperty($address1, 'city', 'Example City');
        $this->setPrivateProperty($address1, 'zipCode', 12345);

        $address2 = new Address();
        $this->setPrivateProperty($address2, 'id', 2);
        $this->setPrivateProperty($address2, 'number', 456);
        $this->setPrivateProperty($address2, 'streetType', 'Another Street Type');
        $this->setPrivateProperty($address2, 'streetName', 'Another Street Name');
        $this->setPrivateProperty($address2, 'city', 'Another City');
        $this->setPrivateProperty($address2, 'zipCode', 67890);

        $company = new Company();
        $this->setPrivateProperty($company, 'id', 1);
        $this->setPrivateProperty($company, 'name', 'Example Company');
        $this->setPrivateProperty($company, 'sirenNumber', 123456789);
        $this->setPrivateProperty($company, 'registrationCity', 'Company City');
        $this->setPrivateProperty($company, 'registrationDate', new \DateTimeImmutable('2023-06-01'));
        $this->setPrivateProperty($company, 'legalForm', $legalForm);

        $addresses = new ArrayCollection([$address1, $address2]);
        $this->setPrivateProperty($company, 'addresses', $addresses);

        $expectedData = [
            'id' => 1,
            'name' => 'Example Company',
            'sirenNumber' => 123456789,
            'registrationCity' => 'Company City',
            'registrationDate' => '2023-06-01',
            'legalForm' => 'Example Legal Form',
            'addresses' => [
                [
                    'id' => 1,
                    'number' => 123,
                    'streetType' => 'Example Street Type',
                    'streetName' => 'Example Street Name',
                    'city' => 'Example City',
                    'zipCode' => 12345,
                ],
                [
                    'id' => 2,
                    'number' => 456,
                    'streetType' => 'Another Street Type',
                    'streetName' => 'Another Street Name',
                    'city' => 'Another City',
                    'zipCode' => 67890,
                ],
            ],
        ];

        $serializedData = CompanySerializer::serialize($company);

        $this->assertSame($expectedData, $serializedData);
    }
}
