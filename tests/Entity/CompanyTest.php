<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Address;
use App\Entity\Company;
use App\Entity\LegalForm;
use PHPUnit\Framework\TestCase;

class CompanyTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $company = new Company();

        $company->setName('Company');
        $this->assertSame('Company', $company->getName());

        $company->setSirenNumber(123456789);
        $this->assertSame(123456789, $company->getSirenNumber());

        $company->setRegistrationCity('City');
        $this->assertSame('City', $company->getRegistrationCity());

        $registrationDate = new \DateTimeImmutable();
        $company->setRegistrationDate($registrationDate);
        $this->assertSame($registrationDate, $company->getRegistrationDate());

        $address1 = new Address();
        $address2 = new Address();

        $company->addAddress($address1);
        $company->addAddress($address2);

        $addresses = $company->getAddresses();
        $this->assertCount(2, $addresses);
        $this->assertTrue($addresses->contains($address1));
        $this->assertTrue($addresses->contains($address2));
        $this->assertSame($company, $address1->getCompany());
        $this->assertSame($company, $address2->getCompany());

        $company->removeAddress($address1);
        $this->assertCount(1, $addresses);
        $this->assertFalse($addresses->contains($address1));
        $this->assertTrue($addresses->contains($address2));
        $this->assertNull($address1->getCompany());
        $this->assertSame($company, $address2->getCompany());

        $legalForm = new LegalForm();
        $company->setLegalForm($legalForm);
        $this->assertSame($legalForm, $company->getLegalForm());

        $company->setLegalForm(null);
        $this->assertNull($company->getLegalForm());
    }
}
