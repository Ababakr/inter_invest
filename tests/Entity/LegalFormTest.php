<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Company;
use App\Entity\LegalForm;
use PHPUnit\Framework\TestCase;

class LegalFormTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $legalForm = new LegalForm();

        $legalForm->setLabel('Legal Form');
        $this->assertSame('Legal Form', $legalForm->getLabel());

        // Test with Company objects
        $company1 = new Company();
        $company2 = new Company();

        $legalForm->addCompany($company1);
        $legalForm->addCompany($company2);

        $companies = $legalForm->getCompanies();
        $this->assertCount(2, $companies);
        $this->assertTrue($companies->contains($company1));
        $this->assertTrue($companies->contains($company2));
        $this->assertSame($legalForm, $company1->getLegalForm());
        $this->assertSame($legalForm, $company2->getLegalForm());

        $legalForm->removeCompany($company1);
        $this->assertCount(1, $companies);
        $this->assertFalse($companies->contains($company1));
        $this->assertTrue($companies->contains($company2));
        $this->assertNull($company1->getLegalForm());
        $this->assertSame($legalForm, $company2->getLegalForm());
    }
}
