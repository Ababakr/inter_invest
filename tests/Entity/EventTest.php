<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Company;
use App\Entity\Event;
use PHPUnit\Framework\TestCase;

class EventTest  extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $event = new Event();

        $event->setEventType('Event');
        $this->assertSame('Event', $event->getEventType());

        $event->setEntity('Entity');
        $this->assertSame('Entity', $event->getEntity());

        $changes = ['field1' => 'value1', 'field2' => 'value2'];
        $event->setChanges($changes);
        $this->assertSame($changes, $event->getChanges());

        $company = new Company();
        $event->setCompany($company);
        $this->assertSame($company, $event->getCompany());
    }
}
