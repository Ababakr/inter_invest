<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Address;
use App\Entity\Company;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $address = new Address();

        $address->setNumber(123);
        $this->assertSame(123, $address->getNumber());

        $address->setStreetType('Avenue');
        $this->assertSame('Avenue', $address->getStreetType());

        $address->setStreetName('Street');
        $this->assertSame('Street', $address->getStreetName());

        $address->setCity('City');
        $this->assertSame('City', $address->getCity());

        $address->setZipCode(12345);
        $this->assertSame(12345, $address->getZipCode());

        $company = new Company();
        $address->setCompany($company);
        $this->assertSame($company, $address->getCompany());

        $address->setCompany(null);
        $this->assertNull($address->getCompany());
    }
}
