# Inter Invest Test

## Installation

Pour installer le projet, suivez ces étapes :

1. Exécutez les commandes suivantes à la racine du projet pour construire et démarrer Docker : `docker-compose build` et `docker-compose up -d`.
2. Exécutez la commande suivante pour récupérer l'ID du conteneur app : `docker ps`, puis utilisez la commande `docker exec -it {id_container} bash` pour accéder au conteneur app.
3. Installez les dépendances : `composer install`.
4. Configurez la base de données : `php bin/console doctrine:database:create`.
5. Effectuez les migrations : `php bin/console doctrine:migrations:migrate`.
6. Pour importer les données des formes juridiques depuis un fichier CSV, exécutez la commande suivante : `php bin/console app:import:legal-form legalForm.csv`

Assurez-vous d'avoir la ligne de connexion suivante dans votre fichier `.env` ou `.env.local` :

```dotenv
DATABASE_URL="mysql://root:root@db:3306/inter_invest?serverVersion=8.0.32&charset=utf8mb4"
